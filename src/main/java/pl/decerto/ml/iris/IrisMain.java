package pl.decerto.ml.iris;

import lombok.extern.slf4j.Slf4j;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.records.reader.impl.transform.TransformProcessRecordReader;
import org.datavec.api.split.FileSplit;
import org.datavec.api.transform.TransformProcess;
import org.datavec.api.transform.analysis.DataAnalysis;
import org.datavec.api.transform.schema.Schema;
import org.datavec.api.transform.transform.normalize.Normalize;
import org.datavec.local.transforms.AnalyzeLocal;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.evaluation.classification.Evaluation;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.SplitTestAndTrain;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.List;

@Slf4j
public class IrisMain {

	private static final long SEED = 12345L;
	private static final long EPOCHS = 500;
	private static final int CLASSES = 3;
	private static final int INPUT = 4;

	private interface Columns {

		String ID = "Id";
		String SEPAL_LENGTH = "SepalLengthCm";
		String SEPAL_WIDTH = "SepalWidthCm";
		String PETAL_LENGTH = "PetalLengthCm";
		String PETAL_WIDTH = "PetalWidthCm";
		String SPECIES = "Species";
	}

	interface Categories {

		List<String> SPECIES = List.of("Iris-setosa", "Iris-versicolor", "Iris-virginica");
	}

	public static void main(String[] args) throws Exception {

		//CSV schema
		Schema schema = new Schema.Builder()
				.addColumnInteger(Columns.ID)
				.addColumnDouble(Columns.SEPAL_LENGTH)
				.addColumnDouble(Columns.SEPAL_WIDTH)
				.addColumnDouble(Columns.PETAL_LENGTH)
				.addColumnDouble(Columns.PETAL_WIDTH)
				.addColumnCategorical(Columns.SPECIES, Categories.SPECIES)
				.build();

		log.info(schema.toString());

		//Load data file
		FileSplit inputSplit = new FileSplit(new File("src/main/resources/data/iris.csv"));
		CSVRecordReader csvRecordReader = new CSVRecordReader(1);
		csvRecordReader.initialize(inputSplit);

		//Local data analysis
		DataAnalysis analysis = AnalyzeLocal.analyze(schema, csvRecordReader);

		//Transforming schema for further processing
		TransformProcess transformProcess = new TransformProcess.Builder(schema)
				.removeColumns(Columns.ID)
				.normalize(Columns.SEPAL_LENGTH, Normalize.MinMax, analysis)
				.normalize(Columns.SEPAL_WIDTH, Normalize.MinMax, analysis)
				.normalize(Columns.PETAL_LENGTH, Normalize.MinMax, analysis)
				.normalize(Columns.PETAL_WIDTH, Normalize.MinMax, analysis)
				.categoricalToInteger(Columns.SPECIES)
				.build();

		Schema transformedSchema = transformProcess.getFinalSchema();
		log.info(transformedSchema.toString());

		//Reset reader, reuse it for data processing
		csvRecordReader.reset();
		TransformProcessRecordReader transformedRecordReader = new TransformProcessRecordReader(csvRecordReader, transformProcess);
		transformedRecordReader.initialize(inputSplit);

		//Build data reader on top of transformed network with batch size
		RecordReaderDataSetIterator dataIterator = new RecordReaderDataSetIterator.Builder(transformedRecordReader, 150)
				.classification(transformedSchema.getIndexOfColumn(Columns.SPECIES), CLASSES)
				.build();

		//Load and shuffle all of 150 records
		DataSet irisDataSet = dataIterator.next();
		irisDataSet.shuffle();

		//Split 150 record in proportion 70/30 (105/45)
		SplitTestAndTrain splitTestAndTrain = irisDataSet.splitTestAndTrain(.70);
		DataSet trainDataSet = splitTestAndTrain.getTrain();
		DataSet testDataSet = splitTestAndTrain.getTest();

		//Build Dense Neural Net configuration
		MultiLayerConfiguration configuration = new NeuralNetConfiguration.Builder()
				.seed(SEED)
				.activation(Activation.RELU)
				.weightInit(WeightInit.XAVIER)
				.updater(new Nesterovs(0.01, 0.9))
				.l2(1e-4)
				.list()
				.layer(new DenseLayer.Builder()
						.nIn(INPUT)
						.nOut(5)
						.build())
				.layer(new DenseLayer.Builder()
						.nIn(5)
						.nOut(5)
						.build())
				.layer(new OutputLayer.Builder()
						.lossFunction(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
						.nIn(5)
						.nOut(CLASSES)
						.activation(Activation.SOFTMAX)
						.build())
				.backpropType(BackpropType.Standard)
				.build();

		MultiLayerNetwork network = new MultiLayerNetwork(configuration);
		network.init();

		//Start learning, pass train dataset for N (EPOCHS) iterations
		Instant start = Instant.now();
		for (int i = 0; i < EPOCHS; i++) {
			network.fit(trainDataSet);
		}
		log.info("Training duration {}ms", Duration.between(start, Instant.now()).toMillis());

		//Evaluation on test dataset
		INDArray output = network.output(testDataSet.getFeatures());
		Evaluation eval = new Evaluation(CLASSES);
		eval.eval(testDataSet.getLabels(), output);
		log.info(eval.stats());

		test(network, testDataSet);
	}

	private static void test(MultiLayerNetwork network, DataSet dataSet) {

		dataSet.shuffle();
		DataSet row = dataSet.get(0);

		log.info("Features {}", row.getFeatures().toString());
		log.info("Labels {}", row.getLabels().toString());
		INDArray output = network.output(row.getFeatures());
		Categories.SPECIES.forEach(specie -> log.info("{} for {}", String.format("%.2f", output.getDouble(Categories.SPECIES.indexOf(specie))), specie));
	}
}
